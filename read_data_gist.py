from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from sklearn import linear_model
from extract_data_V import extract_data2, extract_label2
from skimage import transform
from os.path import join
def read_data():
   file_dir ="/home/vd/csc411-a3/train_output"
   file_name ='800.npy'
   filepath = join(file_dir,file_name) 
   
   #Load inputs
   inputs_t = np.load(filepath)
   print ('input shape is ',inputs_t.shape)
   #Load train dimension
   train_s=0
   train_e=700  
   #Load valid dimension
   valid_s=700
   valid_e=750
   #Load test dimension
   test_s=750
   valid_e=800

  
   x_train = inputs_t[train_s:train_e,:]
   x_valid = inputs_t[train_s:train_e,:];
   x_test = inputs_t[test_s:test_e,:];
  
   label = extract_label2();
   y_train = label[train_s:train_e];
   y_valid = label[valid_s:valid_e];
   return x_train, y_train, x_valid, y_valid, x_test

read_data()

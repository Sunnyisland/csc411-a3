from __future__ import division
from __future__ import print_function
import os
import glob
import sys
import gzip
#import tensorflow as tf;
import numpy as np
from PIL import Image
from numpy import dtype
from _ast import Num
import pca_img
from matplotlib.pyplot import imshow
import gist
from os import listdir, mkdir
from os.path import  exists, isdir, join 

def extract_data2(num_images=7000, num_pixels=3072):
  #num_images = 10
  num_pixels=128*128*3
  #path='/home/simrandeep/workspace/cs411_a3/csc411-a3/train'
  path='/home/ubuntu/csc411-a3/public_test/'
  output_dir='/home/ubuntu/csc411-a3/public_test_output/'
  path_os, dirs, files = os.walk(path).next()
  num_images = len(files)
  print (num_images)
  #print (files)
  if(not exists(output_dir)):
     mkdir(output_dir)
  jpg_files_path = glob.glob(os.path.join(path, '*.[jJ][pP][gG]'))
  input_data = np.zeros(shape=(num_images,960)) #960 is the dimension given by gist extraction
 # print ('input data ',input_data.shape)
  
  for filename in jpg_files_path:
    #print (filename)
    index = int(filename[-9:-4].lstrip("0"))
  #  print ('index ', index)#3-D
    im = Image.open(filename)
    data = np.asarray(im).astype(np.uint8)
    desc = gist.extract(data)
    input_data[index-1]=np.copy(desc)
    #print (input_data[index])
  np.save(join(output_dir,str(num_images)+'.npy'), input_data)
  return input_data


def extract_label2(num_output = 8):
    r = np.genfromtxt('train.csv', delimiter=',', dtype=None, names=True)
    #print(r.shape)
    #print(r)
    target = r['Label']
    return target

#extract_data2();

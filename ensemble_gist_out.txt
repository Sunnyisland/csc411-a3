here
Predicted model accuracy_GaussianProcess: 0.38
('accuracy is ', 0.44)
Predicted model accuracy_MLP: 0.44
('accuracy is ', 0.5)
Predicted model accuracy_RForest: 0.28
('accuracy is ', 0.44)
Predicted model accuracy_AdaBoost_DT: 0.32
('accuracy is ', 0.29999999999999999)
Predicted model accuracy_DT: 0.4
[5 5 1 1 3 2 1 3 1 5 2 5 2 5 5 1 1 1 2 2 1 1 5 2 1 2 1 3 5 2 2 2 5 5 5 1 2
 5 1 1 5 2 2 1 5 5 2 1 1 1]
('accuracy is ', 0.38)
Predicted model accuracy_GaussianNB: 0.32
[3 3 2 3 3 3 1 3 1 2 6 3 5 5 5 4 4 1 3 6 4 5 5 2 5 6 3 5 5 6 3 5 5 3 3 3 5
 5 5 1 4 3 5 1 5 5 6 3 1 1]
('accuracy is ', 0.28000000000000003)
Predicted model accuracy_SVM: 0.28
[5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5
 5 5 5 5 5 5 5 5 5 5 5 5 5]
('accuracy is ', 0.32000000000000001)
Predicted model accuracy_SVM_LN: 0.42
[5 3 1 5 3 2 1 1 1 5 5 2 5 5 2 1 1 1 1 2 3 5 5 2 5 2 1 3 5 1 2 5 5 5 3 2 5
 5 5 1 1 2 1 1 5 5 2 3 1 1]
('accuracy is ', 0.41999999999999998)
Predicted model accuracy LoRegression: 0.36
[5 3 1 5 5 2 1 1 1 5 5 1 5 5 5 1 1 5 1 2 3 5 5 2 5 2 1 5 5 1 2 5 5 5 2 5 5
 5 2 1 5 2 5 1 5 5 2 1 1 1]
('accuracy is ', 0.38)
Predicted model accuracy kNN: 0.36
('accuracy is ', 0.41999999999999998)
Predicted model accuracy OnevsRest: 0.36
[5 3 1 2 3 3 2 1 1 5 2 2 5 5 5 1 1 5 1 1 2 5 5 1 5 2 1 3 5 1 2 5 5 5 3 2 5
 1 5 2 3 2 1 1 5 5 5 3 1 1]
('accuracy is ', 0.44)
('accuracy is ', 0.40000000000000002)

from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from sklearn import linear_model
from extract_data_V import extract_data2,extract_label2
from read_data import *
def logRegres(x_train,y_train,x_valid,y_valid,x_test):
   clf = linear_model.LogisticRegression(C=0.5, solver = 'newton-cg', multi_class='multinomial')
   clf.fit(x_train,y_train);
   #dec = clf.decision_function([[1]])
   #print (dec.shape[1])
   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy LoRegression_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy LoRegression: "+ str(accuracy))
   y_valid = clf.predict(x_valid)
   y_test = clf.predict(x_test)
   #print (y_test)
   return y_test, y_valid

#x_train, y_train, x_valid, y_valid, x_test = read_data()


#y_test = logRegres(x_train, y_train, x_valid, y_valid, x_test)
#print (y_test)

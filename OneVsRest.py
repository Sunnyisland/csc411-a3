from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from sklearn import linear_model
from extract_data_V import extract_data2,extract_label2
from read_data import *
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC

def OneVsRest(x_train,y_train,x_valid,y_valid,x_test):
   clf = OneVsRestClassifier(LinearSVC(random_state=0));
   clf.fit(x_train,y_train);
   #dec = clf.decision_function([[1]])
   #print (dec.shape[1])
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy OnevsRest: "+ str(accuracy))
   y_test = clf.predict(x_test)
   print (y_test)
   return y_test


#x_train, y_train, x_valid, y_valid, x_test = read_data()


#y_test = OneVsRest(x_train, y_train, x_valid, y_valid, x_test)
#print (y_test)

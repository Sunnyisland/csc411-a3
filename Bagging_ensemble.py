from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from extract_data_V import extract_data2,extract_label2
from read_data import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier

def Bagging_kNN(x_train, y_train, x_valid, y_valid, x_test):
   clf =  BaggingClassifier(KNeighborsClassifier(n_neighbors=50))
   clf.fit(x_train,y_train);
   #dec = clf.decision_function([[1]])
   #print (dec.shape[1])
   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy_Bagging_kNN_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy_Bagging_kNN: "+ str(accuracy))
   y_test = clf.predict(x_test)
   #print(y_test)
   return y_test, y_valid

#x_train, y_train, x_valid, y_valid, x_test = read_data()

#y_test = Bagging_kNN(x_train, y_train, x_valid, y_valid, x_test)

#print (y_test)


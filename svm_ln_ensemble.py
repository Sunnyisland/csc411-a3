from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from extract_data_V import extract_data2,extract_label2
from read_data import *

def svm_ln(x_train, y_train, x_valid, y_valid, x_test):
   clf = SVC(kernel ='linear', decision_function_shape='ovo');
   clf.fit(x_train,y_train);
   #dec = clf.decision_function([[1]])
   #print (dec.shape[1])
   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy_SVM_LN_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy_SVM_LN: "+ str(accuracy))
   y_valid = clf.predict(x_valid)
   y_test = clf.predict(x_test)
   #print(y_test)
   return y_test, y_valid
#x_train, y_train, x_valid, y_valid, x_test = read_data()

#y_test = svm_ln(x_train, y_train, x_valid, y_valid, x_test)

#print (y_test)


from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from extract_data_V import extract_data2,extract_label2
from read_data import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

def AdaBoost_DT(x_train, y_train, x_valid, y_valid, x_test):
   clf =  AdaBoostClassifier()
   clf.fit(x_train,y_train);

   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy_AdaBoost_DT_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   y_valid = clf.predict(x_valid)
   print ("Predicted model accuracy_AdaBoost_DT: "+ str(accuracy))
   y_test = clf.predict(x_test)
   #print(y_test)
   return y_test, y_valid

#x_train, y_train, x_valid, y_valid, x_test = read_data()

#y_test = AdaBoost_DT(x_train, y_train, x_valid, y_valid, x_test)

#print (y_test)


import numpy as np
from sklearn import neighbors, datasets, decomposition    
from extract_data_V import extract_data2,extract_label2
from read_data import *
#from ensemble import acc
import gist

def run_knn(x_train, y_train, x_valid, y_valid, x_test):
   # Create an instance of KNeighborsClassifier and then fit training data
   clf = neighbors.KNeighborsClassifier(n_neighbors=10)
   clf.fit(x_train, y_train)
   # Make class predictions for all observations in X
   #Z = clf.predict(V)
   # Compare predicted class labels with actual class labels
   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy kNN_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy kNN: "+ str(accuracy))
   # Add a row of predicted classes to y-array for ease of comparison
   #A = np.vstack([yv, Z])
   #print(A)
   y_valid = clf.predict(x_valid)
   y_test = clf.predict(x_test)
   #print (y_test)
   return y_test, y_valid


#x_train, y_train, x_valid, y_valid, x_test =read_data()
#x_feature=gist.extract(x_train)
#print (x_feature)
#print (x_train.shape)
#print ('x_valid shape')
#label = extract_label2()
#target = label[750:800]
#y_test= run_knn(x_train, y_train, x_valid, y_valid, x_test)

#print (y_test)
#print ('target ', target)
#print (sum(y_test==target).astype('float')/y_test.shape[0])

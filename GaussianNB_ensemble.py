from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from extract_data_V import extract_data2,extract_label2
from read_data import *
from sklearn.naive_bayes import GaussianNB

def GaussianNB_ensemble(x_train, y_train, x_valid, y_valid, x_test):
   clf = GaussianNB();
   clf.fit(x_train,y_train);
   #dec = clf.decision_function([[1]])
   #print (dec.shape[1])
   accuracy_train=clf.score(x_train,y_train)
   print ("Predicted model accuracy_GaussianNB_train: "+ str(accuracy_train))
   accuracy=clf.score(x_valid,y_valid)
   print ("Predicted model accuracy_GaussianNB: "+ str(accuracy))
   y_valid = clf.predict(x_valid)
   y_test = clf.predict(x_test)
   #print(y_test)
   return y_test, y_valid
#x_train, y_train, x_valid, y_valid, x_test = read_data()

#y_test = GaussianNB_ensemble(x_train, y_train, x_valid, y_valid, x_test)

#print (y_test)


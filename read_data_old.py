from __future__ import print_function
import numpy as np
from sklearn.svm import SVC
from sklearn import linear_model
from extract_data_V import extract_data2,extract_label2
from skimage import transform
def read_data():
   inputs_t = extract_data2(800)
   print (inputs_t.shape)
   x_train = inputs_t[:700,:]
   x_valid = inputs_t[700:750,:];
   label = extract_label2();
   y_train = label[:700];
   y_valid = label[700:750];
   x_test = inputs_t[750:800,:];
   return x_train, y_train, x_valid, y_valid, x_test

read_data()

from __future__ import print_function
import numpy as np
from sklearn import *
from extract_data import extract_data2,extract_label2, extract_data2_valid, extract_label2_valid


#X=extract_data2(7000)
#pca = decomposition.PCA(n_components=800)
#pca.fit(X)
#X = pca.transform(X)

V1 = extract_data2_valid(970)
pca1_x = decomposition.PCA(n_components=800)
pca1_x.fit(V1)
V1 = pca1_x.transform(V1)

#tt=extract_label2()
#y = tt[:7000];

#tt1=extract_label2_valid(970)
#yv = tt[:970];


inputs_t = extract_data2(7000)
X = inputs_t[:6000,:];
pca = decomposition.PCA(n_components=800)
pca.fit(X)
X = pca.transform(X)

V = inputs_t[6000:,:];
pca1 = decomposition.PCA(n_components=800)
pca1.fit(V)
V = pca1.transform(V)

tt = extract_label2();
y = tt[:6000];
yv = tt[6000:];



clf = svm.SVC(decision_function_shape='ovr');
clf.fit(X,y);
#dec = clf.decision_function([[1]])
#print (dec.shape[1])
ccuracy=clf.score(V,yv)
print ("Predicted model accuracy: "+ str(ccuracy))

y1 = clf.predict(V1);
print(y1.shape)
print(y1)

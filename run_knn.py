import numpy as np
from sklearn import neighbors, datasets, decomposition    
from extract_data import extract_data2,extract_label2, extract_data2_valid, extract_label2_valid

# Load iris data from 'datasets module'
#iris = datasets.load_iris()
#   Get data-records and record-labels in arrays X and y
#X=extract_data2(7000)
#pca = decomposition.PCA(n_components=800)
#pca.fit(X)
#X = pca.transform(X)

#V = extract_data2_valid(970)
#pca1 = decomposition.PCA(n_components=800)
#pca1.fit(V)
#V = pca1.transform(V)

#tt=extract_label2()
#y = tt[:7000];

#tt1=extract_label2_valid(970)
#yv = tt[:970];

inputs_t = extract_data2(7000)
X = inputs_t[:6000,:];
pca = decomposition.PCA(n_components=800)
pca.fit(X)
X = pca.transform(X)

V = inputs_t[6000:,:];
pca1 = decomposition.PCA(n_components=800)
pca1.fit(V)
V = pca1.transform(V)
tt = extract_label2();
y = tt[:6000];
yv = tt[6000:];

print(X.shape)
print(V.shape)
print(y.shape)
print(yv.shape)
# Create an instance of KNeighborsClassifier and then fit training data
clf = neighbors.KNeighborsClassifier(n_neighbors=5)
clf.fit(X, y)
# Make class predictions for all observations in X
#Z = clf.predict(V)
# Compare predicted class labels with actual class labels
#accuracy=clf.score(X,y)
#print ("Predicted model accuracy: "+ str(accuracy))
ccuracy=clf.score(V,yv)
print ("Predicted model accuracy: "+ str(ccuracy))
# Add a row of predicted classes to y-array for ease of comparison
#A = np.vstack([yv, Z])
#print(A)
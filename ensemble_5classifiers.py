#install  python-scipy: sudo apt-get install python-scipy
from sklearn.utils import shuffle
from scipy.stats import mode
from nn_ensemble import *
from svm_ensemble import *
from run_knn_ensemble import *
from read_data import *
from logRegression_ensemble import *
from OneVsRest_ensemble import *
from GaussianNB_ensemble import *
from GaussianProcess_ensemble import *
from DecisionTree_ensemble import *
from RandomForest_ensemble import *
from svm_ln_ensemble import *
from AdaBoost_ensemble import *
from MLP_ensemble import *
from Bagging_ensemble import *
import time
import os

def ensemble(*args):
   result_array=args[0][np.newaxis] #to make 1D aray to 2D array

   #print len(args)
   len_a = len(args)
   for i in range (1,len_a):
     #print (i)
     result_array=np.concatenate((result_array, args[i][np.newaxis]),axis=0) #axis=1 is for column
   #print result_array, result_array.shape
   ensemble_y=mode(result_array,axis=0)[0][0]
   #print ensemble_y, ensemble_y.shape
   return ensemble_y.T

def acc(output, target=None):
   label = extract_label2()
   if (target==None):
       target = label[6300:7000]
       #target = label[300:330]
   print ('output is ', output)
   print ('target is ', target)
   above =  sum(output==target).astype('float')
   below = target.shape[0]
   #print ('sum and below ', above, below)
   acc = above/below
   print ('accuracy of validation is ', acc)

start_time= time.time()

#print ('here')
x_train, y_train, x_valid, y_valid, x_test=read_data()

x_train, y_train = shuffle(x_train,y_train)

n = x_train.shape[0]/10
print ('n is ', n)
#Bagging_kNN
#y_BaggingkNN, y_BaggingkNN_valid = Bagging_kNN(x_train, y_train, x_valid, y_valid, x_test)
#acc(y_BaggingkNN)

#GaussianProcess:
#y_GaussianProcess, y_GaussianProcess_valid = GaussianProcess(x_train[0:5*n,:], y_train[0:5*n], x_valid, y_valid, x_test)
#y_GaussianProcess_2, y_GaussianProcess_valid_2 = GaussianProcess(x_train[5*n:10*n,:], y_train[5*n:10*n], x_valid, y_valid, x_test)
y_GaussianProcess, y_GaussianProcess_valid = GaussianProcess(x_train[0:2*n,:], y_train[0:2*n], x_valid, y_valid, x_test)
y_GaussianProcess_2, y_GaussianProcess_valid_2 = GaussianProcess(x_train[2*n:4*n,:], y_train[2*n:4*n], x_valid, y_valid, x_test)
print ('Y_Gau',y_GaussianProcess_valid.shape)
#acc(y_GaussianProcess)

#MLP_ensemble:

#y_MLP, y_MLP_valid = MLP(x_train[0:5*n,:],y_train[0:5*n], x_valid, y_valid, x_test)
#y_MLP_2, y_MLP_valid_2 = MLP(x_train[5*n:10*n,:],y_train[5*n:10*n], x_valid, y_valid, x_test)
y_MLP, y_MLP_valid = MLP(x_train[2*n:4*n,:],y_train[2*n:4*n], x_valid, y_valid, x_test)
y_MLP_2, y_MLP_valid_2 = MLP(x_train[4*n:6*n,:],y_train[4*n:6*n], x_valid, y_valid, x_test)
print ('Y_MLP ', y_MLP_valid.shape)
#acc(y_MLP)

#RandomForest
#y_RForest, y_RForest_valid = RForest(x_train[0:5*n,:], y_train[0:5*n], x_valid, y_valid, x_test)
#y_RForest_2, y_RForest_valid_2 = RForest(x_train[5*n:10*n,:], y_train[5*n:10*n], x_valid, y_valid, x_test)
y_RForest, y_RForest_valid = RForest(x_train[4*n:6*n,:], y_train[4*n:6*n], x_valid, y_valid, x_test)
y_RForest_2, y_RForest_valid_2 = RForest(x_train[6*n:8*n,:], y_train[6*n:8*n], x_valid, y_valid, x_test)
print ('Y_RF ', y_RForest_valid.shape)
#acc(y_RForest)


#DecisionTree
#y_DTree, y_DTree_valid = DTree(x_train[6*n:8*n,:], y_train[6*n:8*n], x_valid, y_valid, x_test)
#y_DTree_2, y_DTree_valid_2 = DTree(x_train[8*n:10*n,:], y_train[8*n:10*n], x_valid, y_valid, x_test)
#acc(y_DTree)
#print ('[x_train[3*n:4*n,:]',x_train[3*n:4*n,:])
#GaussianNB:
#y_GaussianNB, y_GaussianNB_valid = GaussianNB_ensemble(x_train[1*n:3*n,:], y_train[1*n:3*n], x_valid, y_valid, x_test)
#y_GaussianNB_2, y_GaussianNB_valid_2 = GaussianNB_ensemble(x_train[3*n:5*n,:], y_train[3*n:5*n], x_valid, y_valid, x_test)
#acc(y_GaussianNB)
#print ('x_train[4*n:5*n,:]',x_train[4*n:5*n,:])
#NN:
#y_NN, y_NN_valid = main()

#SVM
#y_svm, y_svm_valid = svm(x_train, y_train, x_valid, y_valid, x_test)
#acc(y_svm)
#SVM ln
y_svm_ln, y_svm_ln_valid = svm_ln(x_train[6*n:8*n,:], y_train[6*n:8*n], x_valid, y_valid, x_test)
y_svm_ln_2, y_svm_ln_valid_2 = svm_ln(x_train[8*n:10*n,:], y_train[8*n:10*n], x_valid, y_valid, x_test)
print ('Y O_svm_l;n',y_svm_ln_valid.shape)
#acc(y_svm_ln)

#LogReg
#y_logReg, y_logReg_valid = logRegres(x_train[5*n:7*n,:], y_train[5*n:7*n], x_valid, y_valid, x_test)
#y_logReg_2, y_logReg_valid_2 = logRegres(x_train[7*n:9*n,:], y_train[7*n:9*n], x_valid, y_valid, x_test)
#print ('Y O_logReg',y_logReg_valid.shape)
#acc(y_logReg)

#kNN
#y_knn, y_knn_valid = run_knn(x_train[7*n:8*n,:], y_train[7*n:8*n], x_valid, y_valid, x_test)
#y_knn, y_knn_valid = run_knn(x_train[0:5*n,:], y_train[0:5*n], x_valid, y_valid, x_test)
#y_knn_2, y_knn_valid_2 = run_knn(x_train[5*n:10*n,:], y_train[5*n:10*n], x_valid, y_valid, x_test)
#y_knn, y_knn_valid = run_knn(x_train[7*n:9*n,:], y_train[7*n:9*n], x_valid, y_valid, x_test)
#y_knn_2, y_knn_valid_2 = run_knn(x_train[8*n:10*n,:], y_train[8*n:10*n], x_valid, y_valid, x_test)
#print ('Y O_knn	',y_knn_valid.shape)
#acc(y_knn)

#OneVsRest
#y_OneVsRest, y_OneVsRest_valid = OneVsRest(x_train[8*n:9*n,:], y_train[8*n:9*n], x_valid, y_valid, x_test)
#y_OneVsRest, y_OneVsRest_valid = OneVsRest(x_train[0:5*n,:], y_train[0:5*n], x_valid, y_valid, x_test)
#y_OneVsRest_2, y_OneVsRest_valid_2 = OneVsRest(x_train[5*n:10*n,:], y_train[5*n:10*n], x_valid, y_valid, x_test)
y_OneVsRest, y_OneVsRest_valid = OneVsRest(x_train[0*n:2*n,:], y_train[0*n:2*n], x_valid, y_valid, x_test)
y_OneVsRest_2, y_OneVsRest_valid_2 = OneVsRest(x_train[8*n:10*n,:], y_train[8*n:10*n], x_valid, y_valid, x_test)
print ('Y O_OneVsRest	',y_OneVsRest_valid.shape)
#acc(y_OneVsRest)

#AdaBoostDT:
#y_AdaBoostDT, y_AdaBoostDT_valid= AdaBoost_DT(x_train[9*n:10*n,:], y_train[9*n:10*n], x_valid, y_valid, x_test)
#y_AdaBoostDT, y_AdaBoostDT_valid= AdaBoost_DT(x_train[0:5*n,:], y_train[0:5*n], x_valid, y_valid, x_test)
#y_AdaBoostDT_2, y_AdaBoostDT_valid_2= AdaBoost_DT(x_train[5*n:10*n,:], y_train[5*n:10*n], x_valid, y_valid, x_test)
#y_AdaBoostDT, y_AdaBoostDT_valid= AdaBoost_DT(x_train[2*n:4*n,:], y_train[2*n:4*n], x_valid, y_valid, x_test)
#y_AdaBoostDT_2, y_AdaBoostDT_valid_2= AdaBoost_DT(x_train[8*n:10*n,:], y_train[8*n:10*n], x_valid, y_valid, x_test)
#print ('YAD	',y_AdaBoostDT_valid.shape)
#(y_AdaBoostDT)
#y_ensemble_valid = ensemble(y_BaggingkNN_valid, y_MLP_valid, y_GaussianProcess_valid, y_AdaBoostDT_valid, y_RForest_valid, y_DTree_valid, y_GaussianNB_valid, y_svm_ln_valid, y_svm_valid, y_logReg_valid, y_knn_valid, y_OneVsRest_valid, y_NN_valid)
#y_ensemble_valid = ensemble(y_MLP_valid, y_GaussianProcess_valid, y_RForest_valid, y_DTree_valid, y_GaussianNB_valid, y_svm_ln_valid, y_logReg_valid, y_knn_valid, y_OneVsRest_valid, y_AdaBoostDT_valid)
#y_ensemble_valid = ensemble(y_MLP_valid_2, y_GaussianProcess_valid_2, y_RForest_valid_2, y_DTree_valid_2, y_GaussianNB_valid_2, y_svm_ln_valid_2, y_logReg_valid_2, y_knn_valid_2, y_OneVsRest_valid_2, y_AdaBoostDT_valid_2,y_MLP_valid, y_GaussianProcess_valid, y_RForest_valid, y_DTree_valid, y_GaussianNB_valid, y_svm_ln_valid, y_logReg_valid, y_knn_valid, y_OneVsRest_valid, y_AdaBoostDT_valid)
y_ensemble_valid = ensemble(y_MLP_valid_2, y_GaussianProcess_valid_2, y_RForest_valid_2, y_svm_ln_valid_2, y_OneVsRest_valid_2, y_MLP_valid, y_GaussianProcess_valid, y_RForest_valid, y_svm_ln_valid, y_OneVsRest_valid)

acc(y_ensemble_valid)

#y_ensemble = ensemble(y_BaggingkNN, y_MLP, y_GaussianProcess, y_AdaBoostDT, y_RForest, y_DTree, y_GaussianNB, y_svm_ln, y_svm, y_logReg, y_knn, y_OneVsRest, y_NN)
#y_ensemble = ensemble(y_MLP, y_GaussianProcess, y_RForest, y_DTree, y_GaussianNB, y_svm_ln, y_logReg, y_knn, y_OneVsRest,  y_AdaBoostDT)
y_ensemble = ensemble(y_MLP, y_GaussianProcess, y_RForest, y_svm_ln, y_OneVsRest, y_MLP_2, y_GaussianProcess_2, y_RForest_2, y_svm_ln_2, y_OneVsRest_2)
#print ('y_ensemble test is ', y_ensemble)
np.savetxt("prediction_7000_split2_5classifiers_3.csv", y_ensemble,fmt="%d", delimiter=",")

print (" ----%s seconds ----"% (time.time()-start_time))
#os.system('play --no-show-progress --null --channels 1 synth %s sine %f' % ( a, b))


